<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>changePw_PwUpdatedMsg</name>
   <tag></tag>
   <elementGuidId>06c3d625-5f7b-4f13-aff0-5c2f03ac8719</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.alert.alert-warning.alert-dismissable.getAccAlert</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='updatePasswordForm']/div/div/div[2]/div[2]/div/div/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>alert alert-warning alert-dismissable getAccAlert</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
					×
					Your password has been changed</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;updatePasswordForm&quot;)/div[@class=&quot;addressBookLeftRightBox&quot;]/div[@class=&quot;addressBookRightBox&quot;]/div[@class=&quot;myaccount_MyProfile_pw clearfix myaccount_reset_password&quot;]/div[@class=&quot;form-wrapper&quot;]/div[@class=&quot;provide&quot;]/div[@class=&quot;item&quot;]/div[@class=&quot;global-alerts&quot;]/div[@class=&quot;alert alert-warning alert-dismissable getAccAlert&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='updatePasswordForm']/div/div/div[2]/div[2]/div/div/div/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Change Password'])[4]/following::div[7]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Change Password'])[3]/following::div[10]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Please enter the required field'])[1]/preceding::div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Please enter the required field'])[2]/preceding::div[5]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Your password has been changed']/parent::*</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/div/div/div/div</value>
   </webElementXpaths>
</WebElementEntity>
