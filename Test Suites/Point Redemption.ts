<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Point Redemption</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>a7f71b95-a4d8-42c2-81df-8d8cc8a925c3</testSuiteGuid>
   <testCaseLink>
      <guid>97c1ead5-dadc-4e1b-9351-64f064b6e9e3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Tests/HD/PT - HD - Visa</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>05be820c-e427-4cb8-8c51-763e6544ca48</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Tests/CC/PT - CCE - Visa</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>afcb49e0-5994-40ce-9501-b3959e42db3a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment Tests/CC/PT - CCS - Visa</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
