import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import java.util.concurrent.ThreadLocalRandom as Keyword
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import com.kms.katalon.core.logging.KeywordLogger as KeywordLogger

'Generate random number'
String randomNumber = String.valueOf(Math.abs(new Random().nextInt() % (99999999 - 11111111)) + 11111111)

'Declare strings'
String memberEmail = (('autotesting-' + randomNumber) + GlobalVariable.BU) + '@yopmail.com'

String memberName = 'Auto Testing Member'

String memberMobile = '09' + randomNumber

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.defaultURL)

if (GlobalVariable.testingLang == 'en') {
    WebUI.mouseOver(findTestObject('GlobalObjects/globalObject_langPicker'))

    WebUI.enhancedClick(findTestObject('GlobalObjects/globalObject_langPicker_EN'))
}

'Go to my account'
WebUI.enhancedClick(findTestObject('Home/home_MyAccountBtn'))

//Registration
'click register button'
WebUI.enhancedClick(findTestObject('Login/login_RegisterButton'))

'Enter name with randomly generated number'
WebUI.setText(findTestObject('Registration/registration_member_lastName'), memberName)

//if (GlobalVariable.testingLang == 'vn') {
//    WebUI.enhancedClick(findTestObject('Registration/registration_languages'))
//
//    WebUI.enhancedClick(findTestObject('Registration/registration_languages_EN_vn'))
//}
WebUI.enhancedClick(findTestObject('Registration/registration_gender'))

if (GlobalVariable.testingLang == 'en') {
    WebUI.enhancedClick(findTestObject('Registration/registration_gender_male_en'))
} else if (GlobalVariable.testingLang == 'vn') {
    WebUI.enhancedClick(findTestObject('Registration/registration_gender_male_vn'))
}

WebUI.enhancedClick(findTestObject('Registration/registration_BirthDay'))

WebUI.enhancedClick(findTestObject('Registration/registration_BirthDay1'))

WebUI.enhancedClick(findTestObject('Registration/registration_BirthMonth'))

WebUI.enhancedClick(findTestObject('Registration/registration_BirthMonth1'))

WebUI.enhancedClick(findTestObject('Registration/registration_BirthYear'))

WebUI.enhancedClick(findTestObject('Registration/registration_BirthYear1'))

'Paste email'
WebUI.sendKeys(findTestObject('Registration/registraion_EmailField'), memberEmail)

'Paste email again'
WebUI.sendKeys(findTestObject('Registration/registration_EmailVerificationField'), memberEmail)

'Enter mobile'
WebUI.sendKeys(findTestObject('Registration/registration_MobileField'), memberMobile)

'Enter pw'
WebUI.sendKeys(findTestObject('Registration/registration_PasswordField'), GlobalVariable.defaultPassword)

'Enter pw again'
WebUI.sendKeys(findTestObject('Registration/registration_PwVerificationField'), GlobalVariable.defaultPassword)

'submit'
WebUI.enhancedClick(findTestObject('Registration/registration_SubmitButton'))

WebUI.delay(5)

WebUI.setText(findTestObject('Registration/registration_otp'), GlobalVariable.defaultOTP)

WebUI.enhancedClick(findTestObject('Registration/registration_submitOTP'))

'check registration successful'
if (GlobalVariable.testingLang == 'en') {
    WebUI.verifyTextPresent(GlobalVariable.reg_Success_Welcome, false)

    WebUI.verifyTextPresent(GlobalVariable.reg_Success_MemberText, false)
} else if (GlobalVariable.testingLang == 'vn') {
    WebUI.verifyTextPresent(GlobalVariable.reg_Success_Welcome_vn, false)

    WebUI.verifyTextPresent(GlobalVariable.reg_Success_MemberText_vn, false)
}

GlobalVariable.tempLogin = memberEmail

WebUI.delay(5)

//Address Book
WebUI.enhancedClick(findTestObject('Home/home_MyAccountBtn'))

'click the address book link'
WebUI.enhancedClick(findTestObject('AccountSummary/account_AddressBook'))

'add address'
WebUI.enhancedClick(findTestObject('AddressBook/addressBook_AddAddress'))

'Enter name with randomly generated number'
WebUI.setText(findTestObject('AddressBook/addressBook_LastNameField'), memberName)

'Enter mobile'
WebUI.setText(findTestObject('AddressBook/addressBook_MobileField'), memberMobile)

WebUI.setText(findTestObject('AddressBook/addressBook_StreetName'), randomNumber)

WebUI.enhancedClick(findTestObject('AddressBook/addressBook_ProvinceDropdown'), FailureHandling.STOP_ON_FAILURE)

WebUI.enhancedClick(findTestObject('AddressBook/addressBook_Province1'), FailureHandling.STOP_ON_FAILURE)

WebUI.enhancedClick(findTestObject('AddressBook/addressBook_DistrictDropdown'), FailureHandling.STOP_ON_FAILURE)

WebUI.enhancedClick(findTestObject('AddressBook/addressBook_District1'), FailureHandling.STOP_ON_FAILURE)

WebUI.enhancedClick(findTestObject('AddressBook/addressBook_WardDropdown'), FailureHandling.STOP_ON_FAILURE)

WebUI.enhancedClick(findTestObject('AddressBook/addressBook_Ward1'), FailureHandling.STOP_ON_FAILURE)

WebUI.enhancedClick(findTestObject('AddressBook/addressBook_UpdateAddressBtn'))

'check the name was displayed correctly'
WebUI.verifyTextPresent(memberName, false)

'check the mobile is displayed correctly'
WebUI.verifyTextPresent(memberMobile, false)

'edit address'
WebUI.click(findTestObject('AddressBook/addressBook_EditAddressBtn'))

WebUI.setText(findTestObject('AddressBook/addressBook_StreetName'), randomNumber + ' edited')

'update address'
WebUI.enhancedClick(findTestObject('AddressBook/addressBook_UpdateAddressBtn'))

WebUI.verifyTextPresent('edited', false)

'delete address'
WebUI.enhancedClick(findTestObject('AddressBook/addressBook_DeleteAddressBtn'))

WebUI.verifyTextNotPresent(memberMobile, false)

KeywordLogger logger = new KeywordLogger()

logger.logInfo(memberEmail)

