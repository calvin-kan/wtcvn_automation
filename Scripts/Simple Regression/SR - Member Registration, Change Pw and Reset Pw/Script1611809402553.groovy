import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import java.util.concurrent.ThreadLocalRandom as Keyword
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import com.kms.katalon.core.logging.KeywordLogger as KeywordLogger

KeywordLogger logger = new KeywordLogger()

'Generate random number'
String randomNumber = String.valueOf(Math.abs(new Random().nextInt() % (9999999 - 1111111)) + 1111111)

'Declare strings'
String memberEmail = (('autotesting-' + randomNumber) + GlobalVariable.BU) + '@yopmail.com'

String memberFirstName = 'Auto Testing'

String memberLastName = 'Member'

String memberMobile = '9' + randomNumber

String addressFiller = 123456

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.defaultURL)

'Go to my account'
WebUI.enhancedClick(findTestObject('Home/home_MyAccountBtn'))

//Registration
'enhancedClick register button'
WebUI.enhancedClick(findTestObject('Login/login_RegisterButton'))

WebUI.enhancedClick(findTestObject('Registration/registration_signUpAsMember'))

'Enter name with randomly generated number'
WebUI.setText(findTestObject('Registration/registration_member_firstName'), memberFirstName)

'Enter name with randomly generated number'
WebUI.setText(findTestObject('Registration/registration_member_lastName'), memberLastName)

WebUI.enhancedClick(findTestObject('Registration/registration_BirthYear'))

WebUI.enhancedClick(findTestObject('Registration/registration_BirthYear1900'))

WebUI.enhancedClick(findTestObject('Registration/registration_BirthMonth'))

WebUI.enhancedClick(findTestObject('Registration/registration_BirthMonth1'))

'Paste email'
WebUI.setText(findTestObject('Registration/registraion_EmailField'), memberEmail)

'Paste email again'
WebUI.setText(findTestObject('Registration/registration_EmailVerificationField'), memberEmail)

'Enter mobile'
WebUI.setText(findTestObject('Registration/registration_MobileField'), memberMobile)

'Enter pw'
WebUI.setText(findTestObject('Registration/registration_PasswordField'), GlobalVariable.defaultPassword)

'Enter pw again'
WebUI.setText(findTestObject('Registration/registration_PwVerificationField'), GlobalVariable.defaultPassword)

WebUI.setText(findTestObject('Registration/registration_HouseNo'), addressFiller)

WebUI.setText(findTestObject('Registration/registration_StreetName'), addressFiller)

WebUI.setText(findTestObject('Registration/registration_UnitNo'), addressFiller)

WebUI.setText(findTestObject('Registration/registration_PostalCode'), addressFiller)

'submit'
WebUI.enhancedClick(findTestObject('Registration/registration_SubmitButton'))

WebUI.sendKeys(findTestObject('Registration/registration_otp'), GlobalVariable.defaultOTP)

WebUI.enhancedClick(findTestObject('Registration/registration_submitOTP'), FailureHandling.STOP_ON_FAILURE)

WebUI.enhancedClick(findTestObject('Checkout/checkout_autherizeChkbox'))

WebUI.enhancedClick(findTestObject('Checkout/checkout_PayBtn'))

WebUI.enhancedClick(findTestObject('PaymentGateway/paymentGateway_confirmPerInfoBtn'))

WebUI.enhancedClick(findTestObject('PaymentGateway/paymentGateway_visaCardTypeRdBtn'))

WebUI.setText(findTestObject('PaymentGateway/paymentGateway_cardNo'), GlobalVariable.visaCard_cardNo)

WebUI.selectOptionByValue(findTestObject('PaymentGateway/paymentGateway_expiryMonth'), GlobalVariable.visaCard_expiryMonth, 
    false)

WebUI.selectOptionByValue(findTestObject('PaymentGateway/paymentGateway_expiryYear'), GlobalVariable.visaCard_expiryYear, 
    false)

WebUI.sendKeys(findTestObject('PaymentGateway/paymentGateway_cvv'), GlobalVariable.visaCard_cvv)

WebUI.enhancedClick(findTestObject('PaymentGateway/paymentGateway_confirmCardDetails'))

WebUI.enhancedClick(findTestObject('PaymentGateway/paymentGateway_payBtn'))

WebUI.setText(findTestObject('PaymentGateway/paymentGateway_visaPw'), GlobalVariable.visaCard_otp)

WebUI.enhancedClick(findTestObject('PaymentGateway/paymentGateway_submitVisaPw'))

'check thank you page heading visible'
WebUI.verifyTextPresent(GlobalVariable.thankYouPageHeading, false)

WebUI.verifyElementPresent(findTestObject('AccountSummary/account_AccountSummaryTitle'), 3)

'change pw'
WebUI.enhancedClick(findTestObject('AccountSummary/account_changePw'))

WebUI.setText(findTestObject('ChangePassword/changePw_oldPw'), GlobalVariable.defaultPassword)

WebUI.setText(findTestObject('ChangePassword/changePw_newPw'), GlobalVariable.changePassword)

WebUI.setText(findTestObject('ChangePassword/changePw_confirmNewPw'), GlobalVariable.changePassword)

WebUI.enhancedClick(findTestObject('ChangePassword/changePw_updatePwBtn'))

WebUI.verifyElementVisible(findTestObject('ChangePassword/changePw_PwUpdatedMsg'))

'mouse over the my account btn'
WebUI.mouseOver(findTestObject('Home/home_MyAccountBtn'))

'click the logout btn'
WebUI.enhancedClick(findTestObject('Home/home_logoutBtn'))

WebUI.enhancedClick(findTestObject('Home/home_MyAccountBtn'))

WebUI.setText(findTestObject('Login/login_usernameField'), memberEmail)

WebUI.setText(findTestObject('Login/login__passwordField'), GlobalVariable.changePassword)

'click login btn'
WebUI.enhancedClick(findTestObject('Login/login_loginButton'))

'mouse over the my account btn'
WebUI.mouseOver(findTestObject('Home/home_MyAccountBtn'))

'click the logout btn'
WebUI.enhancedClick(findTestObject('Home/home_logoutBtn'))

WebUI.enhancedClick(findTestObject('Home/home_MyAccountBtn'))

'forget pw'
WebUI.enhancedClick(findTestObject('Login/login_forgetPw'))

WebUI.enhancedClick(findTestObject('ForgetPassword/forgetPw_emailResetOption'))

WebUI.enhancedClick(findTestObject('ForgetPassword/forgetPw_nextBtn'))

Boolean whileLoopRanOnce = false

Boolean resetPwEmailReceived = false

while (resetPwEmailReceived == false) {
    if (whileLoopRanOnce == true) {
        WebUI.navigateToUrl('https://infwtcsgy6uat.aswatson.net/login/pw/forgetByEmail')
    }
    
    WebUI.setText(findTestObject('ForgetPassword/forgetPw_emailField'), memberEmail)

    WebUI.enhancedClick(findTestObject('ForgetPassword/forgetPw_confirmEmailBtn'))

    WebUI.verifyTextPresent(GlobalVariable.pwResetToEmailMsg, false)

    WebUI.delay(10)

    WebUI.navigateToUrl('http://www.yopmail.com/en/')

    WebUI.setText(findTestObject('YopmailHomePage/yopmail_EmailField'), memberEmail)

    WebUI.enhancedClick(findTestObject('YopmailHomePage/yopmail_CheckInboxBtn'))

    'click the link in the email'
    resetPwEmailReceived = WebUI.verifyElementPresent(findTestObject('YopmailInbox/yopmail_ResetPw'), 3, FailureHandling.OPTIONAL)

    whileLoopRanOnce = true
}

WebUI.enhancedClick(findTestObject('YopmailInbox/yopmail_ResetPw'))

WebUI.switchToWindowIndex(1)

WebUI.setText(findTestObject('CreateNewPassword/newPw_newPw'), GlobalVariable.defaultPassword)

WebUI.setText(findTestObject('CreateNewPassword/newPw_ConfirmPw'), GlobalVariable.defaultPassword)

WebUI.enhancedClick(findTestObject('CreateNewPassword/newPw_Next'))

WebUI.verifyTextPresent(GlobalVariable.changePwSucessfulMsg, false)

WebUI.verifyTextPresent(memberEmail, false)

WebUI.enhancedClick(findTestObject('CreateNewPassword/newPw_LoginNowBtn'))

'fill in login'
WebUI.setText(findTestObject('Login/login_usernameField'), GlobalVariable.defaultLogin)

'fill in pw'
WebUI.setText(findTestObject('Login/login__passwordField'), GlobalVariable.defaultPassword)

'click login btn'
WebUI.click(findTestObject('Login/login_loginButton'))

'check if reached ac summary page'
WebUI.verifyElementText(findTestObject('AccountSummary/account_AccountSummaryTitle'), GlobalVariable.acSummary_Heading)

logger.logInfo(memberEmail)

